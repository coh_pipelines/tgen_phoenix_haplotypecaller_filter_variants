ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_bcftools:1.10.2 as opt_bcftools

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_gatk:4.1.5.0

COPY --from=opt_bcftools /opt /opt

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module

ENTRYPOINT ["/opt/gatk/run.sh"]
